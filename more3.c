//#nomore
#include<stdio.h>
#include<stdlib.h>
#include <termios.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>

int PAGELEN= 20;
#define LINELEN 512
void do_more(FILE*);
int get_input(FILE*);
int calNoOfLines(FILE *);
int totalLines=0,linesPrinted=0;
int get_input(FILE*);
int calNoOfLines(FILE *);
void setInputMode(struct termios tios);
void resetInputMode(struct termios orig);
void winHandler(int);	

int main(int argc, char* argv[])
{
	struct termios tios,orig;
	struct winsize ts;
	ioctl(0,TIOCGWINSZ,&ts);
	signal(SIGWINCH,winHandler);
	PAGELEN=ts.ws_row-1;
	tcgetattr(0,&orig);
	tios=orig;
	setInputMode(tios);
	if (argc == 1)
	{
		do_more(stdin);
		printf("This is the help page\n");
		exit(0);
	}
	int i = 0;
	FILE * fp;
	while (++i < argc)
	{
		fp = fopen(argv[i], "r");
		if (fp == NULL)
		{
			perror("Can't open file");
			exit(1);
		}
		do_more(fp);
		fclose(fp);
	}
	resetInputMode(orig);
	return 0;
}
int calNoOfLines(FILE *fp)
{
	int noOfLines=0;
	char buffer[LINELEN];
	while (fgets(buffer, LINELEN, fp))
	{
		noOfLines++;
	}
	fseek(fp,0,SEEK_SET);
	return noOfLines;
}

void do_more(FILE * fp)
{
	linesPrinted=0;
	totalLines=calNoOfLines(fp);
	totalLines=calNoOfLines(fp);
	int num_of_lines = 0;
	int rv;
	char buffer[LINELEN];
	FILE * fp_tty =fopen("/dev/tty","r");
	while (fgets(buffer, LINELEN, fp))
	{
		fputs(buffer, stdout);
		num_of_lines++;
		linesPrinted++;
		if (num_of_lines == PAGELEN)
		{
			rv = get_input(fp_tty);
			if (rv == 0)
            {
                printf("\033[2K \033[1G");
				break;
            }
			else if (rv == 1)
            {
				printf("\033[2K \033[1G");
				num_of_lines -= PAGELEN;
            }
			else if (rv == 2)
            {
                printf("\033[2K \033[1G");
				num_of_lines -= 1;
            }
			else if (rv == 3)
            {
                printf("\033[2K \033[1G");
				break;
            }
		}
	}
}
void resetInputMode(struct termios orig)
{
	tcsetattr(0,TCSANOW,&orig);
}
void setInputMode(struct termios tios)
{
 tios.c_lflag &= ~(ICANON|ECHO);
 tios.c_cc[VMIN]=1;
 tios.c_cc[VTIME]=1;
 tcsetattr(0,TCSANOW,&tios);
}

void winHandler(int signalNo)
{
	struct winsize ts;
	ioctl(0,TIOCGWINSZ,&ts);
	PAGELEN=ts.ws_row-1;
}
int get_input(FILE * cmdstream)
{
	int c; 
	int percentage=((linesPrinted*1.0)/totalLines)*100;
	printf("\033[7m --more--(%d%) \033[m",percentage);
	c = getc(cmdstream);
	if (c == 'q')
		return 0;
	if (c == ' ')
		return 1;
	if (c == '\n')
		return 2;
	return 3;
}
