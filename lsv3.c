/*
*  Video Lecture: 14
*  Programmer: Arif Butt
*  Course: System Programming with Linux
*  lsv1.c:
*  usage: ./a.out 
         ./a.out dirpath
         ./a.out dir1 dir2 dir3
*/


#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <grp.h>
#include <pwd.h>
extern int errno;
char option=0;
void do_ls(char*);
void showStat(char * dir,char *file);
int main(int argc, char* argv[]){
   if (argc == 1){
         printf("Directory listing of pwd:\n");
      do_ls(".");
   }
   else{
      int i = 0;
      if(argv[i+1][0]=='-')
      {	i++;
      	if(argv[i][1]=='l')
      		option='l';
      	else{ printf("invalid option");
      	exit(0);
      	}
      	
      }
      while(++i < argc){
         printf("Directory listing of %s:\n", argv[i] );
	 do_ls(argv[i]);
      }
   }
   
   return 0;
}
void do_ls(char * dir)
{
   struct dirent *entry;
	DIR *dp = opendir(dir);
	if(dp == NULL)
	{
		fprintf(stderr, "Cannot open directory: %s\n", dir);
		return;
	}
	errno = 0;
	while((entry = readdir(dp)) != NULL)
	{
		if(entry == NULL && errno != 0)
		{
			perror("readdir failed");
			exit(errno);
		}
		else
		{
			if(entry->d_name[0] == '.')
				continue;
			showStat(dir,entry->d_name);
		}
	}
	closedir(dp);
}
char setFileType(int mode)
{

	if((mode & 0170000) == 0010000)
		return 'p';
	else if((mode & 0170000) == 0020000)
		return 'c';
	else if((mode & 0170000) == 0040000)
		return 'd';
	else if((mode & 0170000) == 0060000)
		return 'b';
	else if((mode & 0170000) == 0100000)
		return '-';
	else if((mode & 0170000) == 0120000)
		return  'l';
	else if((mode & 0170000) == 0140000)
		return 's';
	else
		printf("Unknown File Type\n");
	return 'u';
}
	
void setPermissions(char *permSet,int mode)
{
// Determine File Permissions
	//  Owner Permissions
	if((mode & 0000400) == 0000400) permSet[1] = 'r';
	if((mode & 0000200) == 0000200) permSet[2] = 'w';
	if((mode & 0000100) == 0000100) permSet[3] = 'x';
	// Group Permissions
	if((mode & 0000040) == 0000040) permSet[4] = 'r';
	if((mode & 0000020) == 0000020) permSet[5] = 'w';
	if((mode & 0000010) == 0000010) permSet[6] = 'x';
	// Other Permissions	
	if((mode & 0000004) == 0000004) permSet[7] = 'r';
	if((mode & 0000002) == 0000002) permSet[8] = 'w';
	if((mode & 0000001) == 0000001) permSet[9] = 'x';
	// Special Permissions for User
	if((mode & 0004000) == 0004000)
	{
		if(permSet[3] == 'x')
			permSet[3] = 's';
		else
			permSet[3] = 'S';
	}
	// Special Permissions for Groups
	if((mode & 0002000) == 0002000)
	{
		if(permSet[6] == 'x')
			permSet[6] = 's';
		else
			permSet[6] = 'S';
	}
	// Special Permissions for Others
	if((mode & 0001000) == 0001000)
	{
		if(permSet[9] == 'x')
			permSet[9] = 't';
		else
			permSet[9] = 'T';
	}
}
void userName(struct stat info)
{
	struct passwd *pwd = getpwuid(info.st_uid);
	errno = 0;
	if(pwd == NULL)
	{
		if(errno == 0)
			printf("No Record for uid in /etc/passwd file\n");
		else 
			perror("getpwuid failed");
	}
	else
		printf("%s ", pwd->pw_name);
	
}
void groupName(struct stat info)
{
struct group *grp = getgrgid(info.st_gid);
	errno = 0;
	if(grp == NULL)
	{
		if(errno == 0)
			printf("No Record for gid in /etc/group file\n");
		else 
			perror("getgrpid failed");
	}
	else
		printf("%s ", grp->gr_name);
	
}
void showStat(char * dir,char * file)
{
	struct stat info;
	char newfile[100];
	int j = 0, k = 0;
	while(dir[k] != '\0')
		newfile[j++] = dir[k++];
	k = 0;
	newfile[j++] = '/';
	while(file[k] != '\0')
		newfile[j++] = file[k++];
	newfile[j] = '\0';
	int rv = lstat(newfile, &info);
	if(rv == -1)
	{
		perror("stat failed");
		exit(1);
	}
	if(option=='l'){
	int mode = info.st_mode;
	char permSet[11];
	strcpy(permSet,"----------");
	//int mode=info.st_mode;
	permSet[0]=setFileType(info.st_mode);
	//printf("%c sds",setFileType(mode));
	setPermissions(permSet,mode);
	printf("%s ", permSet);
	printf("%2ld ", info.st_nlink);
	userName(info);
	groupName(info);
	printf("%5ld ", info.st_size);
	char *mtime = ctime(&info.st_mtime);
	int i = 4;
	while(i < 16)
		printf("%c",mtime[i++]); }
	printf(" %s\n", file);
}	

